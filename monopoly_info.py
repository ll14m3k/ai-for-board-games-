
## monopoly_info.py
## This file contains specification of all basic info about the game:
## Sequence of board space
## Costs of all properties and additional purchases (houses, hotels)
## Rents


## Figures for road properties:
## Cost to buy the property, 
## rent 0 house, 1 house, 2 house, 3 house, 4 house, hotel
## What is the cost to buy the houses??

LOCATIONS = [                            #   buy the    buy     -------Rents--------------
         ("GO", "go", []),               #   property  house    0h 1h  2h   3h  4h   hotel    
         ("Old Kent Road",         "road", ( 60,      '??',     2, 10, 30,  90, 160, 250) ),
         ("Community Chest",       "cc",   []),
         ("Whitechapel Road",      "road", ( 60,      '??',     4, 20, 60, 180, 320, 450) ),
         ("Income Tax",             "tax",  ( 0,      '??',     200) ),
         ("King’s Cross Station",  "station", (200,   '??',     200) ),
         ("The Angel Islington",   "road", ( 100,     '??',     6, 30, 90, 270, 400, 550) ),
         ("Chance",                "card", []),
         ("Euston Road",           "road"  ( 100,     '??',     6, 30, 90, 270, 400, 550) ),
         ("Pentonville Road",      "road", ( 120,     '??',     8, 40, 100,300, 450, 600) ),
         ("Jail",                  "jail", []),
         ("Pall Mall",             "road"  ( 140,     '??',     10,50,150, 450, 625, 750) ),
         "Electric Company",            
         ("Whitehall",             "road"  ( 140,     '??',     10,50,150, 450, 625, 750) ),
         ("Northumberland Avenue", "road"  ( 140,     '??',     10,50,150, 450, 625, 750) ),
         ("Marylebone Station",    "station", (200,   '??',     200) ),
         ("Bow Street",            "road"  ( 180,     '??',     14,70,200, 550, 750, 950) ),
         ("Comunity Chest",        "cc",   []),
         ("Marlborough Street",    "road"  ( 180,     '??',     14,70,200, 550, 750, 950) ),
         ("Vine Street",           "road"  ( 200,     '??',     16,80,220, 600, 800, 1000)),
         ("Free Parking",          "parking",[]),
         ("The Strand",            "road"  ( 220,     '??',     18,90,250, 700, 875, 1050)),
         ("Chance",                "card", []),
         ("Fleet Street",          "road"  ( 220,     '??',     18,90,250, 700, 875, 1050)),
         ("Trafalgar Square",      "road"  ( 220,     '??',     20,100,300,750, 925, 1100)),
         ("Fenchurch Street Station", "station", (200,'??',     200) ),
         ("Leicester Square",      "road"  ( 260,     '??',     22,110,330, 800,975, 1150)),
         ("Coventry Street",       "road"  ( 260,     '??',     22,110,330, 800,975, 1150)),
         "Water Works",
         ("Piccadilly",            "road"  ( 280,     '??',     22,120,360, 850,1025,1200)),
         ("Go to Jail"             "jail", []),
         ("Regent Street",         "road"  ( 300,     '??',     26,130,390, 900,1100,1275)),
         ("Oxford Street",         "road"  ( 300,     '??',     26,130,390, 900,1100,1275)),
         ("Comunity Chest"         "cc",   []),
         ("Bond Street",            "road" ( 320,     '??',     28,150,450,1000,1200,1400)),
         ("Liverpool Street Station", "station", (200,'??',     200) ),
         ("Chance",                 "card", []),
         ("Park Lane",             "road", ( 350,     '??',	  35,175,500,	1100,1300,1500)),
         ("Super Tax",             "tax",  ( 0,       '??',     100) ),
         ("Mayfair",               "road", ( 400,     '??',	  50,200,600,	1400,1700,2000))
         ]