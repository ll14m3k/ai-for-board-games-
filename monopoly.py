

import random
#import time


def pounds(number):
    return "£" + str(number)


class Player:
      def __init__(self, name, gender, strategy):
             self.name = name
             self.gender = gender
             self.strategy = strategy
             self.game = None
             self.games_won = 0
             self.index = None
             
      
      def display(self, state):
          game_output( "* {:<9} {:>6}   {:>2}  {:>6}   ({})".format( self.name+":", 
                                                   pounds(self.money(state)),
                                                   len(self.properties(state)),
                                                   pounds(self.total_rent(state)),
                                                   int(self.heuristic(state))
                                                 ) )
    
      def money(self,state):
          return state.money[self.index]
          
      def position(self,state):
          return state.positions[self.index]
          
      def space(self,state):
          return state.spaces[self.index]
          
      def properties(self,state):
          #game_output("*state.properties", state.properties)
          return state.properties[self.index]
    
      def heuristic(self, state):
          return self.strategy.heuristic(state, self)
                    
      def __str__(self):
          return self.name
      
      def __repr__(self):
          return self.name 
          
      def owns(self, space):
          return space in self.properties  
        
      def total_rent(self, state):
          return sum( [prop.rent(state) for prop in self.properties(state) ] )
      
      ## The jeopardy of a player in a given state is the fraction of spaces on
      ## the board, which if the player landed on they would lose (because rent
      ## higher than their total money).
      def jeopardy(self, state):
          deadly = 0
          for space in self.game.board.spaces:
              if ( space.owner(state) != self
                   and
                   space.rent(state) > self.money(state)
                 ):
                 deadly += 1
          return deadly / len(self.game.board.spaces) 
      
      

        
class Space:
    def __init__(self, name, cost, base_rent):
        self.name = name
        self.cost = cost
        self.base_rent = base_rent
        self.neighbours = []
        
    def __string__(self):
        return self.name
    
    def owner( self, state):
        for player in state.players:
            if self in player.properties(state):
                return player
        return None
    
    def occupants( self, state):
        return [ player for player in state.players 
                             if state.spaces[player.index] == self]
        
    def display(self, state):
        self.owner(state)
        if self.owner(state) != None:
            ownerstr = self.owner(state).name
        else:
            ownerstr = ""
        if self.set_owned(state):
            doublestr = "*"
        else:
            doublestr = " "
        game_output("| {:<18} | {:>4} | {:>4}{} | {:<7} | ".format(self.name, self.cost, self.rent(state), doublestr, ownerstr), end = "")
        occupantstr = ", ".join([str(x) for x in self.occupants(state)]) 
        game_output("{}".format(occupantstr))
        
    def add_player(self, p):
        self.occupants.append(p)
        
    def remove_player(self, p):
        self.occupants.remove(p)
        
    def set_owned(self, state):
        owner = self.owner(state)
        if owner == None:
            return False
        for p in self.neighbours:
              if p.owner(state) != owner:
                  return False
        return True
        
    def rent(self, state):
        if self.set_owned(state):
            return self.base_rent * 2
        else:
            return self.base_rent



class GameState:
   def __init__(self):
        pass
              
   @staticmethod  
   def startState( game ):
        gs = GameState()
        gs.game = game
        gs.board = Board()
        gs.players = game.players.copy()
        
        gs.positions = [0 for p in gs.players]
        gs.spaces = [gs.board.spaces[p] for p in gs.positions ]
        gs.money = [game.start_money for p in gs.players]
        gs.properties = [ [] for p in gs.players ]
       
        
        gs.round = 1
        gs.current_player_num = 0
        gs.phase = "turn start"
        gs.display = "verbose"
        return gs
        
   def clone( self ):
       newstate = GameState()
       
       newstate.game = self.game
       newstate.board = self.board
       newstate.players = self.players
       
       newstate.positions  = [x for x in self.positions]
       newstate.spaces     = [x for x in self.spaces]
       newstate.money      = self.money.copy()
       newstate.properties = [x.copy() for x in self.properties]
       
       newstate.round              = self.round 
       newstate.current_player_num = self.current_player_num
       newstate.phase              = self.phase
       newstate.display            = self.display
       return newstate
       
       
   ## Calcuate heursitic gain from player going from current state to newstate.
   ## Will be negative if heuristic value goes down.
   def gainFromStateChange( self, player, newstate):
       return player.heuristic(newstate) - player.heuristic(self)
       
   def occupants(self, space):
       occ_list = []
       for i in range(self.game.num_players):
           if self.spaces[i] == space:
               occ_list.append(self.players[i])
       return occ_list
            
   def display_state(self):
       game_output("-----------------------------------------------")
       game_output("| LOCATION           | COST | RENT  | OWNER   | OCCUPANTS" )
       game_output("|---------------------------------------------|" )
       for space in self.board.spaces:
            space.display(self)
       game_output("----------------------------------------------") 
       for player in self.players:
            player.display(self)
    
   def display_phase(self):
       game_output("Round: {},  Player: {},  Phase: {} ({})".format(self.round,
                                                         self.current_player().name,
                                                         self.phase,
                                                         self.phase))
       
   def current_player(self):
       return self.players[self.current_player_num]
   
   def progress(self, display="verbose"):
       player = self.current_player()
       game_output("Phase:", self.phase)
       
       if self.phase == "round start":
          self.display_state()
          game_output( "Round", self.round)
          self.phase = "turn start"
          return
       
       if self.phase == "turn start":
          game_output( player, "to go.")
          if player.properties(self):
              self.phase = "opportunity to sell"
              return
          else:
              self.phase = "roll and move"
              return
      
       if self.phase == "opportunity to sell":
          game_output( player, "has the following properties for sale:")
          props_for_sale = player.properties(self)
          game_output( ", ".join([prop.name for prop in props_for_sale]))
          for prop in props_for_sale:
              offers = [(op, highest_offer_giving_margin_gain(self, op, player, prop, op.strategy.buy_margin))
                        for op in player.opponents]
              offers = [offer for offer in offers if offer[1] and offer[1] > 0]
              if offers == []:
                  game_output("No offers were made to buy {}.".format(prop.name))
    
              for op, offer in offers:
                  game_output( "*** {} offers £{} for {}.".format(op.name, offer, prop.name))
                  
              offer_result_states =  [ (op, offer, 
                                       resultOfTrade(self, player, op, prop, offer)) 
                                       for (op, offer) in offers ]
              offer_result_state_vals = [ (op, offer, result_state, 
                                           player.heuristic(result_state)) 
                                           for (op, offer, result_state) in offer_result_states] 
              
              acceptable_offer_result_state_vals = [ x for x in offer_result_state_vals if x[3] >= player.strategy.sell_margin]

              if acceptable_offer_result_state_vals == []:
                  if len(offers) > 1:
                     game_output(player, "does not accept any of these offers.")
                  else:
                     game_output(player, "does not accept any of this offer.")
                  continue
              
              if len(acceptable_offer_result_state_vals) > 1:
                 acceptable_offer_result_state_vals.sort(key=lambda x: x[3])  
              accepted_offer =  acceptable_offer_result_state_vals[-1]
              buyer = accepted_offer[0]
              amount = accepted_offer[1]
              game_output( "DEAL: {} agrees to sell {} to {} for £{}.".format(player.name, prop.name, buyer.name, amount ))
              
              
              #resultAllvalue = buyer.heuristic(resultAll)
              
          self.phase = "roll and move"
          return
       
       if self.phase == "roll and move":
          player = self.current_player()
          dice_num = random.randint(1,6)
          game_output( player.name, "rolls", str(dice_num)+"!" )
          self.positions[player.index] = (self.positions[player.index] + dice_num)%self.board.num_spaces
          new_space = self.board.spaces[self.positions[player.index]]
          self.spaces[player.index] =  new_space
        
          game_output( player.name, "moves to", new_space.name + "." )

          if new_space.cost == 0:
             game_output("This place cannot be bought.")
             self.phase = "end of turn"
             return
          if new_space.owner(self): ## someone already owns the space
             game_output("This property is owned by {}.".format(new_space.owner(self).name))
             if new_space.owner(self) == player:
                game_output("{} enjoys visiting {}.".format(player.name, new_space.name))
             else:    
                game_output( "{} must pay £{} to {}.".format(player, new_space.rent(self), new_space.owner(self).name) )
                player_money = self.money[player.index] 
                if player_money < new_space.rent(self): ## Player is knocked out!
                   #game_output("!!", player, "cannot pay and is knocked out of the game !!")
                   self.money[new_space.owner(self).index] += player_money
                   game_output( "{} gets £{} (all {}'s remaining money).".format(new_space.owner(self).name, player_money, player.name))
                   self.money[player.index] = 0
                   self.phase = "bankrupcy"
                   return player
                else:    
                   self.money[player.index] -= new_space.rent(self)
                   self.money[new_space.owner(self).index] += new_space.rent(self)
             self.phase = "end of turn"
             return
          else: ## the space is available to buy
             game_output("This property is for sale for {} spondoolies.".format(new_space.cost))
             if player.money(self) < player.space(self).cost:
                game_output( player.name, "cannot afford", player.space(self).name + "." )
                self.phase = "auction"
                return
             else:
                self.phase = "opportunity to buy" 
                return
       
       ### -------------- BUY PHASE            
       if self.phase == "opportunity to buy":   # buying phase
           player = self.current_player()
           space = player.space(self)

           buy_result_state = resultOfBuy( self, player, space, space.cost)
           buy_value = player.heuristic(buy_result_state)
           
           op_buy_states = [ resultOfBuy( self, op, space, space.cost)
                             for op in player.opponents]
           op_buy_values = [player.heuristic(s) for s in op_buy_states]
           worst_op_buy_value = min( op_buy_values ) 
           
           #game_output(player, "evaluates current state as:", player.heuristic(self) )
           #game_output(player, "evaluates the result of buying as:", result_value )
          
           #gain = self.gainFromStateChange( player, buy_result_state )
           #game_output("Heuristic gain from buying:", gain)
           
           ## if player.money(self) < space.cost + player.strategy.reserve: 
           #if gain < 0:
           if True or buy_value < worst_op_buy_value:
              game_output( player.name, "declines to buy", space.name + "." )
              self.phase = "auction"
              return
           game_output( player.name, "buys", space.name + "." )
           (self.properties[player.index]).append(space)
           self.money[player.index] -=  space.cost
           self.phase = "end of turn"
           return
                    
       if self.phase == "auction":
           auction_space = self.spaces[player.index]
           start_index = player.index + 1
           game_output( auction_space.name, "is up for auction.")
           bid_order_players = players[start_index:].copy() + players[0:start_index].copy()
           winner, bid = auction( self, auction_space, 0, bid_order_players)
           game_output( "{} buys {} for £{}.".format(winner, auction_space.name, bid)) 
           (self.properties[winner.index]).append(auction_space)
           self.money[winner.index] -=  bid
           self.phase = "end of turn"
           return
          
       if self.phase == "end of turn":
             if display == "verbose":
                self.display_state()
             self.current_player_num += 1
             self.phase = "turn start"
             if self.current_player_num == len(self.players):
                 self.current_player_num = 0
                 self.round += 1
                 self.phase = "round start"
                  

def resultOfBuy(state, player, space, cost):
       newstate = state.clone()
       (newstate.properties[player.index]).append(space)
       newstate.money[player.index] -= cost
       return newstate
       
def resultOfTrade(state, seller, buyer, space, cost):
       newstate = state.clone()
       (newstate.properties[seller.index]).remove(space)
       (newstate.properties[buyer.index]).append(space)
       newstate.money[seller.index] += cost  
       newstate.money[buyer.index] -= cost  
       return newstate


def auction(state, space, bid, players):
       player = players[0]
       if len(players) == 1:
          return (player, bid)
       if player.money(state) <= bid:
           game_output( player, "passes. (Not enough money to bid)")
           return auction(state, space, bid, players[1:])
       buy_result_state = resultOfBuy( state, player, space, bid)
       buy_value = player.heuristic(buy_result_state)
       op_buy_states = [ resultOfBuy( state, op, space, bid)
                             for op in player.opponents]
       op_buy_values = [player.heuristic(s) for s in op_buy_states]
       worst_op_buy_value = min( op_buy_values )
       if buy_value > worst_op_buy_value:
          rotate = players[1:].copy()
          rotate.append(player)
          game_output( "{} bids {}.".format(player, bid+1) )
          return auction( state, space, bid+1, rotate )
       else:
          game_output( player, "passes.")
          return auction(state, space, bid, players[1:])
           

### This function should return the highest offer in the range low--high (L-H) that a buyer
### of space can make to its owner and make a heuristic gain for at least the given margin

### (A) If a 0 payment does not make the gain margin gm then None is returned.
### (B) If the total money T available makes the margin this will be returned
### If neither (A) nor (B) hold then successively shrink the range by evaluating
### the gain at the midpoint.          
      
def highest_offer_giving_margin_gain(state, buyer, seller, space, margin):
         targetValue = buyer.heuristic(state) + margin
         result0 = resultOfTrade(state, seller, buyer, space, 0)
         result0value = buyer.heuristic(result0)
         if result0value < targetValue:
             return None
         allmoney = buyer.money(state)
         resultAll = resultOfTrade(state, seller, buyer, space, allmoney)
         resultAllvalue = buyer.heuristic(resultAll)
         if resultAllvalue >= targetValue:
            return allmoney 
         return highest_offer_in_range_meeting_target(state, buyer, seller, space, 
                                                          0, allmoney, targetValue)
         
def highest_offer_in_range_meeting_target(state, buyer, seller, space, low, high, targetValue):
         if low + 1 == high:
             return low
         mid = int( (low+high)/2 )
         resultMid = resultOfTrade(state, seller, buyer, space, mid)
         midValue = buyer.heuristic(resultMid)
         if midValue < targetValue:
             return highest_offer_in_range_meeting_target(state, buyer, seller, space, low, mid, targetValue)
         else:
             return highest_offer_in_range_meeting_target(state, buyer, seller, space, mid, high, targetValue)

class Game:
      def __init__(self, players, start_money, max_rounds):
          self.players = players
          self.start_money = start_money
          self.max_rounds = max_rounds
          self.num_players = len(players)
          
          self.state = GameState.startState(self)
          self.board = self.state.board
          for i in range(len(players)):
              players[i].game = self
              players[i].index = i
              players[i].opponents = players[i+1:].copy() + players[:i].copy()

          game_output("** ==== Leodopoly: the Leeds landlords game ==== **")

      def play(self, display="verbose"):
          self.state.display_state()
          
          while self.max_rounds == 0 or self.state.round <= self.max_rounds:
                result = self.state.progress(display)
                if self.state.phase == "bankrupcy":
                    if result.gender == "male":
                        pronoun = "his"
                    else:
                        pronoun = "her"
                    game_output( result, "has lost all", pronoun, "money and is declared BANKRUPT!" )
                    break
                
          game_output("\nThe final state of play:")      
          self.state.display_state()
                
          if  self.state.phase == "bankrupcy":
              game_output("\n* The game ended in round {} due to bankrupcy).".format(self.state.round))
          else:
              game_output("\n* End of game (the full {} rounds have been played).".format(self.max_rounds))
          
          winning_amount = max([player.money(self.state) for player in players])
          winners = [player for player in players if player.money(self.state) == winning_amount]
          if len(winners) == 1:
             winner = winners[0]
             game_output( "* The winner is {} with £{}.".format(winner.name, winning_amount))
          else:
             winners_str = ", ".join([winner.name for winner in winners])
             game_output( "* The winners are {}, who have £{}.".format(winners_str,winning_amount))
          return winners
        
      def check_for_quit(self):
          key = input( "Press <return> to continue, or enter 'q' to quit: ")  
          if key == "q" or key == "Q":
              return True
          return False
  




    
class Board:    
    def __init__( self ):
       self.go =  Space("Millennium Square",   0,  0)
       ## Woodhouse
       wh1 = Space("Woodhouse Street",  60, 20)
       wh2 = Space("Melville Place",  50, 12)
       wh3 = Space("Quarry Mount",  30, 8)
       ## Hyde Park
       hp1 = Space("Hyde Park Road", 200, 60)
       hp2 = Space("Brudenell Road", 120, 40)
       hp3 = Space("Victoria Road", 250, 80)
       ## Headingley
       h1  = Space("Bearpit Gardens", 300,  2)
       h2  = Space("North Lane",    100,   50)
       ## Education
       e1 = Space("Leeds University", 700, 150)
       e2 = Space("Notre Dame", 200, 60)
       
       self.spaces = [ 
               self.go,
               wh1, wh2, wh3,
               e1,
               hp1, hp2, hp3,
               e2,
               h1, h2
             ]
       
       set1 = [wh1, wh2, wh3]
       set2 = [hp1, hp2, hp3]
       set3 = [h1,  h2]
       set4 = [e1,e2]
       self.sets = [set1, set2, set3, set4]
       self.num_spaces = len(self.spaces)
       for propset in self.sets:
           for prop in propset:
               #game_output(prop.name)
               prop.neighbours = propset.copy()
               prop.neighbours.remove(prop)
    
def test_neighbours():
     board = Board()
     for space in board.spaces:
         game_output("{}: {}".format(space.name, ", ".join([s.name for s in space.neighbours])))   
    
    
class Strategy:   
      def __init__(self, rm, opmm, oprm, bm, sm, reserve, reserve_penalty, ja):
          self.rent_mult = rm                # positive multiplier of total rent
          self.opponent_money_mult = opmm    # negative multiplier of total opponents' money
          self.opponent_rent_mult = oprm     # negative multiplier of total opponents' rent
          self.buy_margin = bm               # gain in heuristic required to buy property
          self.sell_margin = sm              # gain in heuristic required to sell property
          self.reserve = reserve                  # minimum reserve cash
          self.reserve_penalty = reserve_penalty  # negative applied if money lower than reserve 
          self.jeopardy_aversion = ja        # negative multiplier of jeopardy
          ## Jeopardy is calculated as the fraction of spaces owned by other plyers, whose
          ## rent is more than the players money.
          
      def heuristic(self, state, player):
          value = player.money(state) 
          value += player.total_rent(state) * player.strategy.rent_mult
          #opponents = [p for p in state.players if p != player]
          value -= sum( [opponent.money(state) for opponent in player.opponents] ) * self.opponent_money_mult
          value -= sum( [opponent.total_rent(state) for opponent in player.opponents] ) * self.opponent_rent_mult
          
          value -= player.jeopardy(state) * self.jeopardy_aversion
          
          if (player.money(state) < self.reserve):
              value -= self.reserve_penalty
          
          return value
     
GAME_OUTPUT = True
def game_output(*args, end="\n"):
    if GAME_OUTPUT:
       print(*args, end=end)    
    
# Strategies
#               rm opmm  oprm  bm   sm   res   respen  jep av
s1 =  Strategy( 0,  0.7,  2,   10,   5,  500,  1000,   10000) #TIGHT BOUNDARY 
s2 =  Strategy( 3, 0,  2,   50,  50,    0,  1200,   20000) #GREEDY BOUNDARY
s3 =  Strategy( 0,   0,   0,    0,   0,   0,     0 ,   0    ) #IRRATIONAL BOUNDARY  
s4 =  Strategy( 3,  0,  3,     10,  10,  100,   500,   10000) #RANDOM TESTER 1
s5 =  Strategy( 2,  0.1,  3,   10,  10,  300,   500,   10000) #RANDOM TESTER 2
s6 =  Strategy( 9,  0,    1,   10,  10,   0 ,   900,   20000) #RANDOM TESTER 3
s7 =  Strategy( 3,  0,    1,   10,  10,   0 ,   900,   20000) #OPTIMAL STRATEGY

    
brandon = Player("Brandon", "male",   s1 )
marya   = Player("Marya",   "female", s1 )
lucia   = Player("Lucia",   "female", s2 )
AI = Player("AI",   "female", s2 )



players = [ brandon, marya, lucia, AI] 

############# Players, number of games, start money, number of rounds, game output     

def test_series(players, num_games, start_money, game_length, game_output=False):
      global GAME_OUTPUT
      GAME_OUTPUT = game_output
      if GAME_OUTPUT == False:
          print("Running wihout game output ...\n")
      games_played = 0
      while games_played < num_games:
            random.shuffle(players)
            game = Game( players, start_money, game_length )
            winners = game.play("no display")
            if len(winners) == 1: ## if there is a unique winner
                winners[0].games_won += 1
                games_played += 1
      GAME_OUTPUT = True
      print("PLAYER     WINS  PERCENT  |   RM  OPM  OPR   BM  SM   RES  RPEN   JEPAV")
      for player in players:
          won = player.games_won
          percent = ((won*100)/num_games)
          s = player.strategy
          print("{:<9} {:>4}    {:>5.2f}%  |  {:>3.1f}  {:>3.1f} {:4.1f}  {:>3} {:>3}  {:>4}  {:>4}  {:>6}"
                 .format(player.name, won, percent,
                         s.rent_mult, 
                         s.opponent_money_mult,s.opponent_rent_mult,
                         s.buy_margin, s.sell_margin,
                         s.reserve, s.reserve_penalty,
                         s.jeopardy_aversion ) )  
             
test_series( players, 1000, 500, 40, game_output=False)